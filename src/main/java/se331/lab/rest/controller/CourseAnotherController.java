package se331.lab.rest.controller;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import se331.lab.rest.mapper.MapperUtil;
import se331.lab.rest.service.CourseAnotherService;
import se331.lab.rest.service.LecturerAnotherService;

@Controller
@Slf4j
public class CourseAnotherController {
    @Autowired

    CourseAnotherService courseService;

    @GetMapping("/course-enrolled/{courseId}")
    public ResponseEntity getCourseBycourseName(@PathVariable String courseId) {
        log.info("the controller is call");
        return ResponseEntity.ok(MapperUtil.INSTANCE.getCourseDto(this.courseService.getCourseBycourseId(courseId)));
    }

    @GetMapping("/courseWithStudentGpa/{num}")
    public ResponseEntity getCourseBycourseName(@PathVariable double num) {
        log.info("Coursewithstudentgpa ");
        return ResponseEntity.ok(MapperUtil.INSTANCE.getCourseDto(this.courseService.getCourseWhichStudentEnrolledMoreThan(num)));
    }

}

