package se331.lab.rest.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import se331.lab.rest.mapper.MapperUtil;
import se331.lab.rest.service.LecturerAnotherService;
import se331.lab.rest.service.StudentAnotherService;

@Controller
@Slf4j
public class StudentAnotherController {
    @Autowired
    StudentAnotherService studentService;
    @GetMapping("/Student/{name}")
    public ResponseEntity getStudentByName(@PathVariable String name) {
        log.info("Search student name/{name}");
        return ResponseEntity.ok(MapperUtil.INSTANCE.getStudentDto(this.studentService.getStudentByName(name)));
    }
    @GetMapping("/StudentWhoseAdvisorNameIs/{name}")
    public ResponseEntity getStudentWhoseAdvisorNameIs(@PathVariable String name) {
        log.info("Search advisorname / {name}");
        return ResponseEntity.ok(MapperUtil.INSTANCE.getStudentDto(this.studentService.getStudentWhoseAdvisorNameIs(name)));
    }
}
