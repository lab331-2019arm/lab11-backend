package se331.lab.rest.service;

import se331.lab.rest.entity.Course;
import se331.lab.rest.entity.Lecturer;

import java.util.List;

public interface CourseAnotherService {
    Course getCourseBycourseId(String courseName);

    List<Course> getCourseWhichStudentEnrolledMoreThan(double enrolledCourses) ;

}
