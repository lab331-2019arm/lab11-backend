package se331.lab.rest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se331.lab.rest.dao.CourseAnotherDao;
import se331.lab.rest.dao.LecturerAnotherDao;
import se331.lab.rest.entity.Course;
import se331.lab.rest.entity.Lecturer;
import se331.lab.rest.entity.Student;

import java.util.ArrayList;
import java.util.List;

@Service
public class CourseAnotherServiceImpl implements CourseAnotherService {
    @Autowired
    CourseAnotherDao courseAnotherDao;
    @Override
    public Course getCourseBycourseId(String courseId) {
        return courseAnotherDao.getCourseBycourseId(courseId);
    }

    @Override
    public List<Course> getCourseWhichStudentEnrolledMoreThan(double enrolledCourses) {
        List<Course> courses = courseAnotherDao.getAllCourse();
        List<Course> output = new ArrayList<>();
        for (Course course:
                courses) {

                    int numstudent = course.getStudents().size();
            if (numstudent < enrolledCourses){
                output.add(course);
            }

        }
        return output;
    }




}

